angular.module('example', [
	'common.fabric',
	'common.fabric.utilities',
	'common.fabric.constants'	
])

.controller('ExampleCtrl', ['$scope', 'Fabric', 'FabricConstants', 'Keypress', function($scope, Fabric, FabricConstants, Keypress) {

	$scope.fabric = {};
	$scope.FabricConstants = FabricConstants;
	
	/*GLOBAL VARS*/
	$scope.initVars = function(){
		$scope.next = false;
		$scope.done = false;
		$scope.currentStep = 0;
		$scope.mediaMode = 0; //0 - image, 1 - video
		$scope.toolMode = 'line'; //line, polygon
		$scope.steps = [
			{
				step:0,
				name:'Jet Area',
				status:false			
			},
			{
				step:1,
				name: 'VC',
				status:false
			},
			{
				step:2,
				name: 'CW',
				status:false
			},
			{
				step:3,
				name: 'EROA',
				status:false
			},
			{
				step:4,
				name: 'Hepatic',
				status:false
			}
		];
	}
	/*GLOBAL VARS*/

	$scope.init = function(){
		$scope.tool = {
			mediaFile:'',
			lineDistance:null,
			n:null,
			xy:null,
			areaMeasured:null,
			m:null,
			vmax:null,
			r:null,
			va:null,
			eroa:null
		};	
		$scope.mediaMode = 0;
		switchTool('line',true);
	};	


	$scope.moveTo = function(k){
		if($scope.steps[k].status){
			var tM = (k == 0)?'polygon':'line';
			var data = angular.copy($scope.steps[k].data);
			var fname = data.mediaFile;    	
    		var ext = fname.split('.').pop().toLowerCase();
    		if(($.inArray(ext, ['avi','mp4']) >= 0)){   
	    		$scope.mediaMode = 1;	    		
	            loadVideo(data.mediaFile);
	        }else{        	
	        	$scope.mediaMode = 0;	    		
	    		prototypefabric.initCanvas(data.mediaFile);        	
	        }
	        $scope.tool = data;
	        switchTool(tM,true);
	        $scope.currentStep = k;
	        $scope.next = true;
		}		
	};	

	$scope.calibrate = function(){
		if($scope.tool.lineDistance && $scope.tool.xy){
			$scope.tool.n = $scope.tool.lineDistance/$scope.tool.xy;		
			var t = 'line';
			if($scope.currentStep == 0){
				t = 'polygon';
			}
			$scope.toolMode = t;
			switchTool(t);
		}else{
			alert('Please draw a line and enter value for calibrate.');			
		}
	}

	$scope.measure = function(){
		if($scope.currentStep == 3 && $scope.tool.va == null){			
			alert('Please enter value for va');							
		}else{			
			$scope.next = true;
		}
	}

	$scope.$watch('tool.areaMeasured', function(newValue, oldValue) {
	  if(newValue){
	  	var m = null;
	  	if($scope.currentStep == 0)
	  		m = $scope.tool.areaMeasured/($scope.tool.n*$scope.tool.n);
	  	else
	  		m = $scope.tool.areaMeasured/($scope.tool.n);

	  	$scope.tool.m = m;
	  	if($scope.currentStep == 2)
	  		$scope.tool.vmax = m;


	  }
	});

	$scope.$watch('tool.lineDistance', function(newValue, oldValue) {
		if($scope.currentStep == 3 && newValue){
			$scope.tool.r = newValue;	
		}
	});

	$scope.$watch('tool.mediaFile', function(newValue, oldValue) {
		if($scope.currentStep == 4 && newValue){
			$scope.steps[$scope.currentStep].data = angular.copy($scope.tool);
			$scope.steps[$scope.currentStep].status = true;			
			$scope.done = true;
		}
	});

	$scope.$watch('tool.va', function(newValue, oldValue) {
		if($scope.currentStep == 3 && newValue){
			var vmax = $scope.steps[2].data.vmax;
			var r = $scope.tool.r;
			var va = newValue;
			$scope.tool.eroa = 2*3.14159*(r*r)*va/vmax;			
		}
	});

	$scope.resetTool = function(mode){		
		var toolMode = mode;		
		if(mode == 'polygon'){
			$scope.tool.m = null;
			$scope.tool.areaMeasured = null;
			$scope.tool.vmax = null;
			$scope.tool.va = null;
			$scope.tool.eroa = null;
			/*Switch to line tool if step is not JET AREA*/
			if($scope.currentStep!=0){
				toolMode = 'line';
			}
		}else{
			$scope.tool = {
				mediaFile:$scope.tool.mediaFile,
				lineDistance:null,
				n:null,
				xy:null,
				areaMeasured:null,
				m:null,
				vmax:null,
				r:null,
				va:null,
				eroa:null
			};
		}		
		switchTool(toolMode);
		$scope.next = false;
	}

	$scope.save = function(){
		$scope.steps[$scope.currentStep].data = angular.copy($scope.tool);
		$scope.steps[$scope.currentStep].status = true;			
		if($scope.currentStep<4)
			$scope.currentStep+=1;

		if($scope.steps[$scope.currentStep].status){
			$scope.moveTo($scope.currentStep);
		}else{			
			$scope.init();	
			$scope.next = false;
		}
	}

	$scope.reset = function(){
		if (confirm("Are you sure all data will be reset?")) {
			$scope.init();
			$scope.initVars();
		}
	}

	$scope.init();
	$scope.initVars();
}]);