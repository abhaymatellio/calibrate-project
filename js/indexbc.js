angular.module('example', [
	'common.fabric',
	'common.fabric.utilities',
	'common.fabric.constants'
])

.controller('ExampleCtrl', ['$scope', 'Fabric', 'FabricConstants', 'Keypress', function($scope, Fabric, FabricConstants, Keypress) {

	$scope.fabric = {};
	$scope.FabricConstants = FabricConstants;
	$scope.currentStep = 0;
	
	$scope.steps = [
		{
			step:0,
			name:'Jet Area',
			status:false			
		},
		{
			step:1,
			name: 'VC',
			status:false
		},
		{
			step:2,
			name: 'CW',
			status:false
		},
		{
			step:3,
			name: 'EROA',
			status:false
		},
		{
			step:4,
			name: 'Hepatic',
			status:false
		}
	];



	//
	// Creating Canvas Objects
	// ================================================================
	$scope.addShape = function(path) {
		$scope.fabric.addShape('http://fabricjs.com/assets/15.svg');
	};

	$scope.addImage = function(image) {
		// $scope.fabric.addImage('http://stargate-sg1-solutions.com/blog/wp-content/uploads/2007/08/daniel-season-nine.jpg');
		$scope.fabric.addImage('http://localhost/angularfabric/VC.png');

	};

	$scope.addImageUpload = function(data) {
		var obj = angular.fromJson(data);
		$scope.addImage(obj.filename);
	};

	//
	// Editing Canvas Size
	// ================================================================
	$scope.selectCanvas = function() {
		$scope.canvasCopy = {
			width: $scope.fabric.canvasOriginalWidth,
			height: $scope.fabric.canvasOriginalHeight
		};
	};

	$scope.setCanvasSize = function() {
		$scope.fabric.setCanvasSize($scope.canvasCopy.width, $scope.canvasCopy.height);
		$scope.fabric.setDirty(true);
		delete $scope.canvasCopy;
	};

	//
	// Init
	// ================================================================
	$scope.init = function() {
		$scope.fabric = new Fabric({
			JSONExportProperties: FabricConstants.JSONExportProperties,
			textDefaults: FabricConstants.textDefaults,
			shapeDefaults: FabricConstants.shapeDefaults,
			json: {}
		});
	};

	$scope.$on('canvas:created', $scope.init);

	Keypress.onSave(function() {
		$scope.updatePage();
	});

	$scope.options = {

	    // Required. Called when a user selects an item in the Chooser.
	    success: function(files) {
	    	var fileData = files[0];
	    	var fname = fileData.name;
	    	console.log(files[0].link);
	    	/*var ext = fname.split('.').pop().toLowerCase();
	    	if(($.inArray(ext, ['avi','mp4']) >= 0)){
	    		$('#onlyvideo').removeClass('hide');
	            loadVideo(files[0].link);
	        }else{
	        	$('#onlyvideo').addClass('hide');
	    		prototypefabric.initCanvas(files[0].link);        	
	        }*/
	        //alert("Here's the file link: " + files[0].link)
	    },

	    // Optional. Called when the user closes the dialog without selecting a file
	    // and does not include any parameters.
	    cancel: function() {

	    },

	    // Optional. "preview" (default) is a preview link to the document for sharing,
	    // "direct" is an expiring link to download the contents of the file. For more
	    // information about link types, see Link types below.
	    linkType: "direct", //"preview" or "direct"

	    // Optional. A value of false (default) limits selection to a single file, while
	    // true enables multiple file selection.
	    multiselect: false, // or true

	    // Optional. This is a list of file extensions. If specified, the user will
	    // only be able to select files with these extensions. You may also specify
	    // file types, such as "video" or "images" in the list. For more information,
	    // see File types below. By default, all extensions are allowed.
	    extensions: ['.png', '.jpg', '.jpeg', '.mp4', '.bmp', '.tif', '.avi'],

	    // Optional. A value of false (default) limits selection to files,
	    // while true allows the user to select both folders and files.
	    // You cannot specify `linkType: "direct"` when using `folderselect: true`.
	    folderselect: false, // or true

	    // Optional. A limit on the size of each file that may be selected, in bytes.
	    // If specified, the user will only be able to select files with size
	    // less than or equal to this limit.
	    // For the purposes of this option, folders have size zero.
	    //sizeLimit: 1024, // or any positive number
	};

	$scope.moveTo = function(step){
		$scope.currentStep = step;
	};

	$scope.openDrop = function(){
		Dropbox.choose($scope.options);
	};

}]);