var min = 99;
var max = 999999;
var polygonMode = true;
var pointArray = new Array();
var lineArray = new Array();
var activeLine;
var activeShape = false;
var canvas;
var backGround;
var video;
var image;
var isLineDraw = true;
var isSecondPoint = false;
var cw,ch;
var isNotRendered = true;
var videoElem;
var frameTime = 1/27;
var dropBoxOptions = {

    // Required. Called when a user selects an item in the Chooser.
    success: function(files) {
    	var fileData = files[0];
    	var fname = fileData.name;    	
    	var ext = fname.split('.').pop().toLowerCase();
    	$('#mediaFile').val(files[0].link).trigger('change');
    	if(($.inArray(ext, ['avi','mp4']) >= 0)){   
    		$('#mediaMode').val(1).trigger('change');
    		isLineDraw = true;
			isSecondPoint = false;
            loadVideo(files[0].link);
        }else{        	
    		$('#mediaMode').val(0).trigger('change');
    		prototypefabric.initCanvas(files[0].link);        	
        }        
    },

    // Optional. Called when the user closes the dialog without selecting a file
    // and does not include any parameters.
    cancel: function() {

    },

    // Optional. "preview" (default) is a preview link to the document for sharing,
    // "direct" is an expiring link to download the contents of the file. For more
    // information about link types, see Link types below.
    linkType: "direct", //"preview" or "direct"

    // Optional. A value of false (default) limits selection to a single file, while
    // true enables multiple file selection.
    multiselect: false, // or true

    // Optional. This is a list of file extensions. If specified, the user will
    // only be able to select files with these extensions. You may also specify
    // file types, such as "video" or "images" in the list. For more information,
    // see File types below. By default, all extensions are allowed.
    extensions: ['.png', '.jpg', '.jpeg', '.mp4', '.bmp', '.tif', '.avi'],

    // Optional. A value of false (default) limits selection to files,
    // while true allows the user to select both folders and files.
    // You cannot specify `linkType: "direct"` when using `folderselect: true`.
    folderselect: false, // or true

    // Optional. A limit on the size of each file that may be selected, in bytes.
    // If specified, the user will only be able to select files with size
    // less than or equal to this limit.
    // For the purposes of this option, folders have size zero.
    //sizeLimit: 1024, // or any positive number
};
var w = $('.image-builder').width();

$('#choosefile').on('click',function(){
	Dropbox.choose(dropBoxOptions);
});

var prototypefabric = new function () {
	this.initCanvas = function (data) {

		/*Clear the canvas*/
		if(canvas)
			canvas.dispose();

		canvas = window._canvas = new fabric.Canvas('c',{ selection: false });
		
		/*ADD IMAGE TO CANVAS*/		
    	canvas.remove(backGround);
    	if(data){
			fabric.Image.fromURL(data, function (img) {

				imgWidth = img.width;
				imgHeight = img.height;				
				/*If image is bigger then container resize it to container*/
				if(imgWidth > w){
					imgHeight = (imgHeight/imgWidth) * w;
					imgWidth = w;
					img.setWidth(w);
					img.setHeight(imgHeight);
				}
				canvas.setWidth(imgWidth);	
				canvas.setHeight(imgHeight);

				backGround = canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
					backgroundImageOpacity: 1,
					backgroundImageStretch: false
				});
				prototypefabric.polygon.clear(pointArray);
				prototypefabric.polygon.drawPolygon(); 
			});	
		}

		var enableTool = ($('#currentStep').val()!=4);
		if(enableTool)
			loadTool('image');
	};
};

function loadTool(type){

	canvas.on('mouse:down', function (options) {
		if(options.target && options.target.id == pointArray[0].id){
			prototypefabric.polygon.generatePolygon(pointArray);
		}
		if(polygonMode){
			prototypefabric.polygon.addPoint(options);

			if(isSecondPoint && isLineDraw){
				prototypefabric.polygon.generateLine(pointArray);
			}
			isSecondPoint = true;
		}
	});
	
	canvas.on('mouse:move', function (options) {
		if(activeLine && activeLine.class == "line"){
			var pointer = canvas.getPointer(options.e);
			activeLine.set({ x2: pointer.x, y2: pointer.y });

			var points = activeShape.get("points");
			points[pointArray.length] = {
				x:pointer.x,
				y:pointer.y
			}
			activeShape.set({
				points: points
			});			
		}
		canvas.renderAll();
	});	

	$('#controls').removeClass('hide');

	if(type == 'video'){
		$('.videoOpt').removeClass('hide');
	}else{
		$('.videoOpt').addClass('hide');
	}	

	//scaleControl.val(1);
	$('#rval').html(1);
	isNotRendered = true;
}

function loadVideo(src){
	if(canvas)
		canvas.dispose();	

	canvas = window._canvas = new fabric.Canvas('c',{ selection: false });

	$('#dropbox_video').find('source').attr('src',src);
	$('#dropbox_video').load();
	videoElem = document.getElementById('dropbox_video');		

	videoElem.addEventListener( "loadedmetadata", function (e) {
	    
	    var vWidth = this.videoWidth;
	    var vHeight = this.videoHeight;

	    if(vWidth > w){
	    	vHeight = (vHeight/vWidth) * w;
			vWidth = w;			
		}

	    $(this).attr('width',vWidth);
	    $(this).attr('height',vHeight);

		video = new fabric.Image(videoElem, {
		  objectCaching: false,
		  selectable:false,
		  hasControls: false,
		  evented: false
		});

		canvas.setWidth(vWidth);
		canvas.setHeight(vHeight);
		canvas.add(video);		
		
		//video.getElement().play();
		
		var enableTool = ($('#currentStep').val()!=4);
		if(enableTool)			
			loadTool('video');

		fabric.util.requestAnimFrame(function render() {
		  canvas.renderAll();
		  fabric.util.requestAnimFrame(render);
		});			

	}, false );

}

$('#play_pause').on('click',function(){
	if(!videoElem.paused){
		videoElem.pause();		
	}else{
		videoElem.play();		
	}
});

function sframe(type){
	if(!videoElem.paused){
		videoElem.pause();		
	}	
	if(type){
		videoElem.currentTime = Math.min(videoElem.duration, videoElem.currentTime + frameTime);
	}else{
		videoElem.currentTime = Math.max(0, videoElem.currentTime - frameTime);
	}		
}

/*$('#toolMode').on('change',function(){
	isLineDraw = ($('#toolMode').val() == 'line');	
	if(pointArray.length)
		prototypefabric.polygon.clear(pointArray); 

    prototypefabric.polygon.drawPolygon();
});*/

function switchTool(flag, dispose = false){
	if(dispose && canvas){		
		/*canvas.dispose();*/
		if(backGround)
			canvas.setBackgroundImage(null);
		
		canvas.clear();
		canvas.setHeight(300);
	}

	isLineDraw = (flag == 'line');	
	
	if(pointArray.length)
		prototypefabric.polygon.clear(pointArray); 

    prototypefabric.polygon.drawPolygon();
}

$('#clear').on('click',function(){
	prototypefabric.polygon.clear(pointArray); 
	prototypefabric.polygon.drawPolygon(); 
});

window.onbeforeunload = function ()
{
 return "";
};	